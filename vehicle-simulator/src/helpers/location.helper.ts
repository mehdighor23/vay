import { generateRandom } from "helpers/number.helper";
import { Location } from "types/location";
import { berlinLocation } from "constants/location.constant";

export function createLocation(): Location {
  return {
    latitude:
      berlinLocation.latitude + parseFloat(`0.00${generateRandom(1000, 9999)}`),
    longitude:
      berlinLocation.longitude +
      parseFloat(`0.00${generateRandom(1000, 9999)}`),
  };
}

export function offsetLocation(location: Location): Location {
  return {
    latitude:
      location.latitude + parseFloat(`0.000${generateRandom(100, 999)}`),
    longitude:
      location.longitude + parseFloat(`0.000${generateRandom(100, 999)}`),
  };
}
