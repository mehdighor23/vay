export function generateRandom(min: number, max: number) {
  const difference = max - min;
  const rand = Math.random();
  return Math.floor(rand * difference) + min;
}
