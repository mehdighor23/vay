import defaultAxios from "axios";
import env from "constants/environment.constant";

const axios = defaultAxios.create({
  baseURL: env.SERVER_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

export default axios;
