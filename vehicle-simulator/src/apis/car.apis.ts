import axios from "configs/axios.config";

export function registerCar(name: string) {
  return axios.post("api/car", { name }).then((res) => res?.data);
}
