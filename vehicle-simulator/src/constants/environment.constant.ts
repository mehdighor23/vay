import "dotenv/config";

const env = {
  SERVER_URL: process.env.SERVER_URL,
  CAR_COUNT: process.env.CAR_COUNT,
};

export default env;
