import faker from "@faker-js/faker";
import { connectKey, updateLocationKey } from "constants/socket.constant";
import { registerCar } from "apis/car.apis";
import env from "constants/environment.constant";
import { io } from "socket.io-client";
import { createLocation, offsetLocation } from "helpers/location.helper";

async function bootstrap() {
  for (const item of [...Array(parseInt(env.CAR_COUNT)).keys()]) {
    const name = faker.name.firstName();

    const car = await registerCar(name);

    let startLocation = createLocation();

    const socket = io(env.SERVER_URL, { reconnection: true });

    socket.on(connectKey, () => {
      setInterval(() => {
        startLocation = offsetLocation(startLocation);

        const payload = {
          ...car,
          latitude: startLocation.latitude,
          longitude: startLocation.longitude,
        };
        socket.emit(updateLocationKey, payload);
      }, 1000);
    });
  }
}

bootstrap();
