# Fleet State

This project was developed with Node js, Socket.io, and React.

## Setup

In the root directory, please run:

### `docker-compose up -d`


After that open [http://localhost:3000](http://localhost:3000) to view the map in the browser.

## Structure

#### Fleet State backend
This is the basic backend that wrote with NodeJs and will be used to create the server and sockets.

### #Vehicle Simulator
This is a NodeJs app that simulates cars and sends location updates every one second to Fleet State backend socket.

#### Web App
a React client that shows live locations of cars on the map. 
