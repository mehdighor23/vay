import { setupMongodb, setupServer, setupSocket } from "setups";

async function bootstrap() {
  try {
    await setupMongodb();
    const server = await setupServer();
    setupSocket(server);
  } catch (e) {
    console.log(e);
  }
}

bootstrap();
