import { getModelForClass, prop } from "@typegoose/typegoose";

export class City {
  @prop({ required: true })
  public name!: string;

  @prop({ required: true })
  public latitude!: number;

  @prop({ required: true })
  public longitude!: number;
}

const CityModel = getModelForClass(City, {
  schemaOptions: { versionKey: false },
});

export default CityModel;
