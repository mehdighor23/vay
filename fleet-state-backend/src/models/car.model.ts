import { prop, getModelForClass } from "@typegoose/typegoose";

export class Car {
  @prop({ required: true })
  public name!: string;
}

const CarModel = getModelForClass(Car, {
  schemaOptions: { versionKey: false },
});

export default CarModel;
