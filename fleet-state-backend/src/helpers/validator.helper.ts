import { validationResult } from "express-validator";
import { NextFunction, Request, Response } from "express";

export default function validate(
  request: Request,
  response: Response,
  next: NextFunction
) {
  const errors = validationResult(request);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }));

  return response.status(422).json({
    errors: extractedErrors,
  });
}
