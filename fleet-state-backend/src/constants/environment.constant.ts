import "dotenv/config";

const env = {
  SERVER_PORT: process.env.SERVER_PORT,
  MONGODB_H0ST: process.env.MONGODB_H0ST,
  MONGODB_PORT: process.env.MONGODB_PORT,
  MONGODB_DATABASE: process.env.MONGODB_DATABASE,
};

export default env;
