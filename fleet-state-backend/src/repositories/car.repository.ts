import CarModel, { Car } from "models/car.model";

export default class CarRepository {
  static create(car: Car) {
    return CarModel.create(car);
  }
}
