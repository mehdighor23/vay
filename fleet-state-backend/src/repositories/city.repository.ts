import CityModel, { City } from "models/city.model";

export default class CityRepository {
  static upsert(city: City) {
    return CityModel.updateOne(
      { name: city.name },
      { $set: { ...city } },
      { upsert: true }
    );
  }
}
