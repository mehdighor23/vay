import { Request, Response } from "express";
import CarRepository from "repositories/car.repository";

export async function register(request: Request, response: Response) {
  const name = request.body.name;

  const car = await CarRepository.create({ name });
  response.send(car);
}
