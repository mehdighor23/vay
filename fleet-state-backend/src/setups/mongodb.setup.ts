import * as mongoose from "mongoose";
import env from "constants/environment.constant";

export default async function setupMongodb() {
  await mongoose.connect(
    `mongodb://${env.MONGODB_H0ST}:${env.MONGODB_PORT}/${env.MONGODB_DATABASE}`
  );
  console.log("MongoDB is ready");
}
