import * as express from "express";
import * as http from "http";
import * as bodyParser from "body-parser";
import env from "constants/environment.constant";
import router from "routes";

export default function setupServer(): Promise<http.Server> {
  return new Promise((resolve) => {
    const app = express();

    const server = http.createServer(app);

    app.use(bodyParser.urlencoded({ extended: false }));

    app.use(bodyParser.json());

    app.use("/api", router);

    server.listen(env.SERVER_PORT, () => {
      console.log(`Server is listening on ${env.SERVER_PORT}`);
      resolve(server);
    });
  });
}
