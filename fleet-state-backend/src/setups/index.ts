import setupServer from "setups/server.setup";
import setupMongodb from "setups/mongodb.setup";
import setupSocket from "setups/socket.setup";

export { setupServer, setupMongodb, setupSocket };
