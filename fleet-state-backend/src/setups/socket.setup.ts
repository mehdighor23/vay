import { Server } from "socket.io";
import * as http from "http";
import { connectionKey, updateLocationKey } from "constants/socket.constant";

export default function setupSocket(server: http.Server) {
  const io = new Server(server, {
    cors: {
      origin: ["http://localhost:3000"],
      credentials: true,
    },
  });
  io.on(connectionKey, (socket) => {
    socket.on(updateLocationKey, (data) => {
      io.emit("locations", data);
    });
  });
}
