import * as express from "express";
import carRouter from "routes/car.route";

const router = express.Router();

const allRoutes = [
  {
    path: "/car",
    router: carRouter,
  },
];

allRoutes.forEach((route) => router.use(route.path, route.router));

export default router;
