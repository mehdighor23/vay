import * as express from "express";
import { body } from "express-validator";
import validate from "helpers/validator.helper";
import { register } from "controllers/car.controller";

const carRouter = express.Router();

carRouter.post("/", body("name").isString(), validate, register);

export default carRouter;
