import Cars from "features/cars";
import GlobalStyle from "core/styles/globalStyles";

const App = (): JSX.Element => {
  return (
    <>
      <GlobalStyle />
      <Cars />
    </>
  );
};

export default App;
