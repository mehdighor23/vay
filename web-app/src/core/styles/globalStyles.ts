import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    height: 100vh;
  }

  #root {
    display: flex;
    height: 100%;
  }
`;

export default GlobalStyle;
