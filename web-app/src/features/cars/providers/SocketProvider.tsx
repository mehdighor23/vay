import {
  createContext,
  ReactElement,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { Socket } from "socket.io-client";
import socket from "features/cars/configs/socket";

export const SocketContext = createContext<{ socket?: Socket }>({});

type Props = {
  children: ReactElement;
};

const SocketProvider = (props: Props): JSX.Element => {
  const { children } = props;

  const socketRef = useRef<Socket>();

  const [isConnected, setConnected] = useState(false);

  useEffect(() => {
    socket.on("connect", () => {
      socketRef.current = socket;
      setConnected(true);
    });
  }, []);

  const value = useMemo(
    () => ({ socket: socketRef.current }),
    [socketRef.current]
  );

  return (
    <>
      {isConnected && socketRef.current ? (
        <SocketContext.Provider value={value}>
          {children}
        </SocketContext.Provider>
      ) : (
        <span>Loading</span>
      )}
    </>
  );
};

export default SocketProvider;
