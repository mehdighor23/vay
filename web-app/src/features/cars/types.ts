export type CarLocation = {
  name: string;
  _id: string;
  latitude: number;
  longitude: number;
};
