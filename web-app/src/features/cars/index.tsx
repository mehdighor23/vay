import SocketProvider from "features/cars/providers/SocketProvider";
import LiveTracking from "features/cars/pages/liveTracking";

export const Test = () => {
  return (
    <SocketProvider>
      <LiveTracking />
    </SocketProvider>
  );
};

export default Test;
