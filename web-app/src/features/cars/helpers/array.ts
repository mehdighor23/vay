export function mergeArray<Item extends { _id: string }>(
  source: Item[],
  newArray: Item[]
): Item[] {
  return [
    ...newArray,
    ...source.filter(
      (item) => !newArray.map((item) => item._id).includes(item._id)
    ),
  ];
}
