import { useContext } from "react";
import { SocketContext } from "features/cars/providers/SocketProvider";

export default function useSocket() {
  const { socket } = useContext(SocketContext);


  return { socket };
}
