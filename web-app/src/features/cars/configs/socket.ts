import { io } from "socket.io-client";

const socket = io("http://localhost:3100", {
  reconnection: true,
  autoConnect: true,
  upgrade: true,
});

export default socket;
