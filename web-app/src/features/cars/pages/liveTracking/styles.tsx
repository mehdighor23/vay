import styled from "styled-components";
import CoreMap from "core/components/Map";

export const Map = styled(CoreMap)`
  margin: 40px;
  flex: 1;
`;
