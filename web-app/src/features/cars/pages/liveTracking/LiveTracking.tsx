import { useEffect, useState } from "react";
import { CarLocation } from "features/cars/types";
import L, { LatLng } from "leaflet";
import { mergeArray } from "features/cars/helpers/array";
import useSocket from "features/cars/hooks/useSocket";
import { Marker } from "react-leaflet";
import * as Styled from "./styles";

const myIcon = new L.Icon({
  iconUrl: require("features/cars/assets/car.png"),
  iconRetinaUrl: require("features/cars/assets/car.png"),
  popupAnchor: [-0, -0],
  iconSize: [65, 50],
});

const LiveTracking = (): JSX.Element => {
  const { socket } = useSocket();

  const [locations, setLocations] = useState<CarLocation[]>([]);

  useEffect(() => {
    socket?.on("locations", (carLocation: CarLocation) => {
      setLocations((prevState) => mergeArray(prevState, [carLocation]));
    });
  }, [socket]);

  return (
    <Styled.Map center={[52.515774, 13.383011]}>
      {locations.map((item) => (
        <Marker
          icon={myIcon}
          key={item._id}
          position={new LatLng(item.latitude, item.longitude)}
        />
      ))}
    </Styled.Map>
  );
};

export default LiveTracking;
